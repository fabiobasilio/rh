﻿using RH.Senai.RH.Dao;
using RH.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Forms
{
    public partial class CadFuncionario : Form
    {
        public CadFuncionario()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("CPF: " + txtCpfFuncionario.Text);
        }

        private void CadFuncionario_Load(object sender, EventArgs e)
        {
            PreencheDados();
        }

        private void PreencheDados()
        {
            // instancia uma dao
            FuncionarioDao dao = new FuncionarioDao();

            // preenche o datagrid view
            dgvFuncionarios.DataSource = dao.Consultar();

            // oculta algumas colunas
            dgvFuncionarios.Columns["ID"].Visible = false;
            dgvFuncionarios.Columns["RG"].Visible = false;

            // limpa a seleção do data grid view
            dgvFuncionarios.ClearSelection();

            // limpa os campos do formulário
            LimparFormulario();

        }

        private void btnSalvarFuncionario_Click(object sender, EventArgs e)
        {
            // instancia um funcionário
            Funcionario funcionario = new Funcionario();

            //atribui dados ao funcionário
            funcionario.Nome = txtNomeFuncionario.Text;
            funcionario.RG = txtRgFuncionario.Text;
            funcionario.Email = txtEmailFuncionario.Text;
            funcionario.Telefone = txtTelefoneFuncionario.Text;
            funcionario.CPF = txtCpfFuncionario.Text;

            // instancia o dao de funcionários
            FuncionarioDao dao = new FuncionarioDao();

            // salva o funcionário no banco de dados
            dao.Salvar(funcionario);

            PreencheDados();

            LimparFormulario();


        } // fim do evento de click

        // métodos do programador
        private void LimparFormulario()
        {
            txtIDFuncionario.Clear();
            txtNomeFuncionario.Clear();
            txtRgFuncionario.Clear();
            txtEmailFuncionario.Clear();
            txtTelefoneFuncionario.Clear();
            txtCpfFuncionario.Clear();
            txtCpfFuncionario.Focus();
        }

        private void dgvFuncionarios_SelectionChanged(object sender, EventArgs e)
        {
            // se alguma linha for selecionada 
            if (dgvFuncionarios.CurrentRow != null)
            {
                // pega o id e coloca no textbox de id
                txtIDFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[0].Value.ToString();
                txtNomeFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[1].Value.ToString();
                txtRgFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[2].Value.ToString();
                txtEmailFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[3].Value.ToString();
                txtTelefoneFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[4].Value.ToString();
                txtCpfFuncionario.Text = dgvFuncionarios.CurrentRow.Cells[5].Value.ToString();
            }
        }

        private void btnExcluirFuncionario_Click(object sender, EventArgs e)
        {
            // verifica se o textbox de ID de Funcionário é nulo ou vazio
            // se sim isso significa que nenhum funcionário foi selecionado na lista

            if (string.IsNullOrEmpty(txtIDFuncionario.Text))
            {
                string msg = "Selecione um funcionário na lista abaixo!";
                string titulo = "Operação não realizada...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            
            
            else
            {
                // instancia um funcionario
                Funcionario funcionario = new Funcionario();

                // cria um id para o funcionário
                long id = 0;

                    if (long.TryParse(txtIDFuncionario.Text, out id))
                    {
                    // atribui o id ao id do funcionário
                    funcionario.ID = id; 
                    }

                    // instancia uma dao
                    FuncionarioDao dao = new FuncionarioDao();

                    // exclui o funcionario
                    dao.Excluir(funcionario);


                // atualiza o data grid view
                PreencheDados();


                }
            }
        }
    } // fim da classe




