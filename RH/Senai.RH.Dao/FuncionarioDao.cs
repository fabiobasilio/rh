﻿using RH.Senai.RH.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RH.Senai.RH.Dao
{
    class FuncionarioDao : IDao<Funcionario>
    {
        // atributos
        // conexão com o banco de dados
        private SqlConnection connection;

        // instrução sql
        private string sql = null;

        // mensagem do messagebox
        private string msg = null;

        // título do messagebox
        private string titulo = null;
        private object mbox;

        // construtor
        public FuncionarioDao()
        {
            // cria uma conexão com o banco de dados
            connection = new ConnectionFactory().GetConnection();
        }

        // métodos
        public List<Funcionario> Consultar()
        {
            // comando sql de consulta
            sql = "SELECT * FROM Funcionario";

            // lista de funcionarios cadastrados
            List<Funcionario> funcionarios = new List<Funcionario>();

            try
            {
                // abre a conexão com o bando de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // cria um leitor de dados
                SqlDataReader leitor = cmd.ExecuteReader();

                // enquanto o leitor tiver dados para ler
                while (leitor.Read())
                {
                    Funcionario funcionario = new Funcionario();
                    funcionario.ID = (long) leitor["IDFuncionario"];
                    funcionario.Nome = leitor["Nome"].ToString();
                    
                    funcionario.RG = leitor["RG"].ToString();
                    funcionario.Email = leitor["Email"].ToString();
                    funcionario.Telefone = leitor["Telefone"].ToString();
                    funcionario.CPF = leitor["Cpf"].ToString();

                    // adiciona o formulário na lista de funcionários
                    funcionarios.Add(funcionario);
                    // fim do while
                }

            }
            catch (SqlException ex)

            {
                msg = "Erro ao consultar os funcionários cadastrados: " + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                connection.Close();
            }
            return funcionarios;

        }

        public void Excluir(Funcionario funcionario)
        {
            // instrução sql
            sql = "DELETE FROM Funcionario WHERE IDFuncionario = @IDFuncionario";
            try
            {
                // abre a conexão com o banco de dados
                connection.Open();

                // cria um comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);

                // adiciona valor ao parâmetro @IDFuncionario
                cmd.Parameters.AddWithValue("@IDFuncionario", funcionario.ID);

                // executa o comando sql no banco de dados
                cmd.ExecuteNonQuery();


                // mensagem de feedback
                msg = "Funcionário" + funcionario.Nome + "excluído com sucesso!";

                // titulo da mensagem de feedback
                titulo = "Sucesso...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            catch (SqlException ex)
            {
                // mensagem de feedback
                msg = "Erro ao excluir o funcionário!" + ex.Message;

                // titulo da mensagem de erro
                titulo = "Erro...";

                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
            finally
            {
                // fecha a conexão com o banco de dados
                connection.Close();
            }
        }

        public void Salvar(Funcionario funcionario)
        {
          try
            {
                sql = "INSERT INTO Funcionario(Nome, Rg, Email, Telefone, Cpf) VALUES (@Nome, @Rg, @Email, @Telefone, @Cpf)";
                // abre uma conexão com o banco de dados
                connection.Open();

                // comando sql
                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.Parameters.AddWithValue("@Nome", funcionario.Nome);
                cmd.Parameters.AddWithValue("@Rg", funcionario.RG);
                cmd.Parameters.AddWithValue("@Email", funcionario.Email);
                cmd.Parameters.AddWithValue("@Telefone", funcionario.Telefone);
                cmd.Parameters.AddWithValue("@Cpf", funcionario.CPF);

                // executa o INSERT
                cmd.ExecuteNonQuery();
                msg = "Funcionario" + funcionario.Nome + " salvo com sucesso!";
                titulo = "Sucesso...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (SqlException ex)
            {
                msg = "Erro ao salvar o funcionário!" + ex.Message;
                titulo = "Erro...";
                MessageBox.Show(msg, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            finally
            {
                connection.Close();

            }
        }
    }
}
